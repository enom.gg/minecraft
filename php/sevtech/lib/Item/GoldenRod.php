<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class GoldenRod extends Item implements Process\Process
{
    ///
    public function process()
    {
        return [
            Process\MetalPress::ROD => [
                GoldIngot::class => 2,
                GoldPlate::class => 2,
            ],
            Process\StoneAnvil::class => GoldPlate::class
        ];
    }
}