<?php

require '../vendor/autoload.php';

use Sevtech\Sevtech;

/**
 *
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Minecraft Materials Cost</title>
    <style type="text/css">
        html,
        body {
            background-color: #00aaee;
            color: #03161d;
            font-family: "DejaVu Sans","Arial",sans-serif;
            font-size: 18pt;
            margin: 0;
            padding: 0;
        }

        a, a:active, a:hover, a:visited {
            color: #03161d;
            /*font-weight: bold;*/
            /*text-decoration: none;*/
        }

        /* --- */

        .flex {
            display: flex;
            align-items: center;
        }

        .flex .flex-item {
            flex-grow: 1;
            text-align: center;
        }

        .link  {
            border: 1px solid #03161d;
            padding: 15px 25px;
        }

        /* --- */

        #wrapper {
            height: 100vh;
        }

        #items {
            margin-left: 25%;
            text-align: left;
        }

        #items a {

        }

        #materials {
            text-align: left;
        }
    </style>
</head>
<body>
    <div id="wrapper" class="flex">
        <div class="flex-item">
            <ul id="items">
                <?php foreach (Sevtech::items() as $item) { ?>
                    <li><a href="<?= $item->href() ?>"><?= $item->name() ?></a></li>
                <?php } ?>
            </ul>
        </div>

        <div class="flex-item">
            <?php if ($item = Sevtech::item()) { // if $_GET[item] ?>
                <h2><?= $item->name() ?></h2>

                <div id="materials">
                    <?php var_dump($item->cost()) ?>

                    <?php foreach ($item->cost() as $proc => $cost) { ?>
                        <h4><?= Sevtech::basename($proc) ?></h4>

                        <dl>
                            <?php foreach ($cost as $material => $quantity) { ?>
                                <dt><?= $material ?></dt>
                                <dd><?= $quantity ?></dd>
                            <?php } // end foreach ($cost) ?>
                        </dl>
                    <?php } // end foreach ($item->cost()) ?>
                </div>
            <?php } // end if $_GET[item] ?>
        </div>
    </div>
</body>
</html>