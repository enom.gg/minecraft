import { wants } from '--/utils'
import SteelIngot from './steel-ingot'

export default class BlockOfSteel {
  constructor () {
    this.name = 'Block of Steel'
  }

  crafting () {
    return wants(9, SteelIngot)
  }
}
