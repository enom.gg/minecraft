import CopperIngot from './copper-ingot'
import IronMechanicalComponent from './iron-mechanical-component'
import IronSheetmetal from './iron-sheetmetal'

export default class LightEngineeringBlock {
  constructor () {
    this.name = 'Light Engineering Block'
    this.yields = 4
  }

  crafting () {
    return [
      IronSheetmetal, IronMechanicalComponent, IronSheetmetal,
      IronMechanicalComponent, CopperIngot, IronMechanicalComponent,
      IronSheetmetal, IronMechanicalComponent, IronSheetmetal
    ]
  }
}
