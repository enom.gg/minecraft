<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class BlackIronSlate extends Item implements Process\Process
{
    ///
    public function process()
    {
        return [
            Process\MetalPress::PLATE => BlackIronIngot::class,
            Process\StoneAnvil::class => BlackIronIngot::class,
        ];
    }
}