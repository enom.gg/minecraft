import ConstantanIngot from './constantan-ingot'
import EngineersHammer from '../tools/engineers-hammer'

export default class ConstantanPlate {
  constructor () {
    this.name = 'Constantan Plate'
  }

  crafting () {
    return [
      ConstantanIngot, EngineersHammer
    ]
  }
}
