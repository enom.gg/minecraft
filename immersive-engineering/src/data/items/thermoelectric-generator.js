import ConstantanPlate from './constantan-plate'
import CopperCoilBlock from './copper-coil-block'
import SteelIngot from './steel-ingot'

export default class ThermoelectricGenerator {
  constructor () {
    this.name = 'Thermoelectric Generator'
  }

  crafting () {
    return [
      SteelIngot, SteelIngot, SteelIngot,
      ConstantanPlate, CopperCoilBlock, ConstantanPlate,
      ConstantanPlate, ConstantanPlate, ConstantanPlate
    ]
  }
}
