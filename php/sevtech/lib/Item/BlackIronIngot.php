<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class BlackIronIngot extends Item implements Process\Crafting
{
    ///
    public function process()
    {
        return [
            Process\AlloyKiln::class => [
                CrushedBlackQuartz::class,
                IronIngot::class,
            ],
            Process\ArcFurnace::class => [
                IronIngot::class,
                CrushedBlackQuartz::class,
            ],
            Process\MetalPress::class => [
                Process\MetalPress::MOLD => Process\MetalPress::UNPACKING,
                BlockOfBlackIron::class,
            ],
        ];
    }
}