export default function clone (v) {
  if (v === null || typeof v !== 'object') return v
  return Array.isArray(v)
    ? v.reduce((a, e) => [...a, clone(e)], [])
    : Object.keys(v).reduce((o, k) => ({ ...o, [k]: clone(v[k]) }), {})
}
