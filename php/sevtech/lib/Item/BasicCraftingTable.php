<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;
use Sevtech\Age;

/**
 *
 */
class BasicCraftingTable extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Age\Two::class => [
                Crafting::class => [
                    BasicComponent::class => 4,
                    BasicCatalyst::class => 1,
                    CraftingTable::class => 2,
                    BlockOfIron::class => 1,
                    BlackIronIngot::class => 1,
                ],
            ],
        ];
    }
}