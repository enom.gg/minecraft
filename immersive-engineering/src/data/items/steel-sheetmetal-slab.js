import SteelSheetmetal from './steel-sheetmetal'

export default class SteelSheetmetalSlab {
  constructor () {
    this.name = 'Steel Sheetmetal Slab'
    this.yields = 6
  }

  crafting () {
    return [
      null, null, null,
      SteelSheetmetal, SteelSheetmetal, SteelSheetmetal,
      null, null, null
    ]
  }
}
