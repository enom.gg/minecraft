import ElectrumIngot from './electrum-ingot'
import SteelMechanicalComponent from './steel-mechanical-component'
import SteelSheetmetal from './steel-sheetmetal'

export default class HeavyEngineeringBlock {
  constructor () {
    this.name = 'Heavy Engineering Block'
    this.yields = 4
  }

  crafting () {
    return [
      SteelSheetmetal, SteelMechanicalComponent, SteelSheetmetal,
      SteelMechanicalComponent, ElectrumIngot, SteelMechanicalComponent,
      SteelSheetmetal, SteelMechanicalComponent, SteelSheetmetal
    ]
  }
}
