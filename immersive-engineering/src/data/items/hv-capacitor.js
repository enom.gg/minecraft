import AluminiumIngot from './aluminium-ingot'
import BlockOfLead from './block-of-lead'
import BlockOfRedstone from './block-of-redstone'
import SteelIngot from './steel-ingot'
import TreatedWoodPlanks from './treated-wood-planks'

export default class HVCapacitor {
  constructor () {
    this.name = 'HV Capacitor'
  }

  crafting () {
    return [
      SteelIngot, SteelIngot, SteelIngot,
      AluminiumIngot, BlockOfLead, AluminiumIngot,
      TreatedWoodPlanks, BlockOfRedstone, TreatedWoodPlanks
    ]
  }
}
