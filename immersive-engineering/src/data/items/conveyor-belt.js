import IronIngot from './iron-ingot'
import Leather from './leather'
import RedstoneDust from './redstone-dust'

export default class ConveyorBelt {
  constructor () {
    this.name = 'Conveyor Belt'
    this.yields = 8
  }

  crafting () {
    return [
      null, null, null,
      Leather, Leather, Leather,
      IronIngot, RedstoneDust, IronIngot
    ]
  }
}
