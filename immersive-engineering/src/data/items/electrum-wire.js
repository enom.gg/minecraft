import ElectrumPlate from './electrum-plate'
import EngineersWireCutters from '../tools/engineers-wire-cutters'

export default class ElectrumWire {
  constructor () {
    this.name = 'Electrum Wire'
  }

  crafting () {
    return [
      ElectrumPlate, EngineersWireCutters
    ]
  }
}
