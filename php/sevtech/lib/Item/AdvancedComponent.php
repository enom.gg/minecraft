<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;
use Sevtech\Age;

/**
 *
 */
class AdvancedComponent extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Age\Three::class => [
                Crafting::class => [
                    GoldenRod::class => 2,
                    BlackIronSlate::class => 1,
                    Luminessence::class => 1,
                ],
            ],
        ];
    }
}