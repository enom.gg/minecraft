import CopperCoilBlock from './copper-coil-block'
import CopperIngot from './copper-ingot'
import IronIngot from './iron-ingot'
import RedstoneDust from './redstone-dust'

export default class ExternalHeater {
  constructor () {
    this.name = 'External Heater'
  }

  crafting () {
    return [
      IronIngot, CopperIngot, IronIngot,
      CopperIngot, CopperCoilBlock, CopperIngot,
      IronIngot, RedstoneDust, IronIngot
    ]
  }
}
