import CopperPlate from './copper-plate'
import EngineersWireCutters from '../tools/engineers-wire-cutters'

export default class CopperWire {
  constructor () {
    this.name = 'Copper Wire'
  }

  crafting () {
    return [
      CopperPlate, EngineersWireCutters
    ]
  }
}
