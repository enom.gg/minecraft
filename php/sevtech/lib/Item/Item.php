<?php

namespace Sevtech\Item;

use Sevtech\Age;
use Sevtech\Collection;
use Sevtech\Process;
use Sevtech\Sevtech;

/**
 *
 */
class Item
{

    /**
     * Print friendly class name.
     *
     * @var string
     */
    public $name;

    /**
     * Contains the list of base materials required to craft.
     *
     * @var \Sevtech\Collection
     */
    protected $cost;

    /**
     * Item constructor.
     */
    public function __construct()
    {
        $this->cost = new Collection();
    }

    /**
     * Returns the class name without its namespace.
     *
     * @return string
     */
    public function basename()
    {
        return Sevtech::basename(static::class);
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function cost()
    {
        $cost = [];

        if ($this instanceof Process\Process) {
            foreach ($this->process() as $age => $processes) {
                // // Processes can have an age scope containing the required materials
                // $age = new \ReflectionClass($age);
                //
                // if ($age->isSubclassOf(Age\Age::class)) {
                //     $age = $age->newInstance();
                // } else {
                //     // No age implies age 0
                //     $processes = $age;
                //     $age = new Age\Age();
                // }

                // $age = new $age();

                var_dump(...[
                    __METHOD__,
                    $age,
                    new $age(),
                    $processes,
                ]);
                die();

                if (!($age instanceof Age\Age)) {
                }

                if (!array_key_exists($age->age(), $cost)) {
                    $cost[$age->age()] = [];
                }


            }
        }

        return $cost;
    }

    // /**
    //  * Returns the full list of base materials required to craft.
    //  *
    //  * @return array
    //  */
    // public function cost()
    // {
    //     if ($this instanceof Process\Process) {
    //         foreach ($this->process() as $class => $process) {
    //             $name = Sevtech::basename($class);
    //
    //             switch ($type = gettype($process)) {
    //                 case 'string':
    //                     $this->cost->add($this->costsOne($process), $name);
    //
    //                     break;
    //                 case 'array':
    //                     $this->cost->add($this->costsMany($process), $name);
    //
    //                     break;
    //                 default:
    //                     throw new \TypeError(__METHOD__ . ' invalid ' . $type);
    //             }
    //         }
    //     }
    //
    //     return $this->cost->all();
    // }

    /**
     * Adds a single item to our costs.
     *
     * @param \Sevtech\Item\Item|string $item Item to add
     * @return array
     */
    protected function costsOne($item)
    {
        return (is_string($item) ? new $item() : $item)->cost();
    }

    /**
     * Adds multiple items to our costs.
     *
     * @param array $items
     * @return array
     */
    protected function costsMany(array $items)
    {
        return [];
    }

    /**
     * Returns the link to the item.
     *
     * @return string
     */
    public function href()
    {
        return '?item=' . $this->basename();
    }

    /**
     * Returns a print friendly name.
     *
     * @return string
     */
    public function name()
    {
        // Prefer manually declared $name property instead of autogenerated name
        return $this->name ?? Sevtech::splitname($this->basename());
    }

    // /**
    //  * Increments a process material cost by one.
    //  *
    //  * @param string $process Process associated with the cost
    //  * @param \Sevtech\Item\Item|string $item Item to add
    //  */
    // protected function processCostsOne(string $process, $item)
    // {
    //     if (is_string($item)) {
    //         $item = new $item();
    //     }
    //
    //     foreach ($item->cost()) {
    //
    //     }
    //
    //
    //
    //     $this->cost[$process][get_class($item)] = 1;
    // }
    //
    // /**
    //  * Returns the list of processed materials required to craft.
    //  *
    //  * @param array $costs Existing cost to add to
    //  * @return array
    //  */
    // protected function processCost()
    // {
    //     foreach ($this->process() as $proc => $mats) {
    //         $this->cost[$proc] = [];
    //
    //         if (is_string($mats)) {
    //             $this->processCostsOne($proc, $mats);
    //         }
    //
    //         if (is_string($mats)) {
    //             // Process requires and yields only one item
    //             $this->cost[$proc][$mats] = ($this->cost[$proc][$mats] ?? 0) + 1;
    //         } else {
    //             foreach ($mats as $mat => $cost) {
    //                 if (is_string($cost)) {
    //                     [$mat, $cost] = [$cost, 1];
    //
    //                     // Process requires a certain amount of one item to craft
    //                     $this->cost[$proc][$mat] = ($this->cost[$proc][$mat] ?? 0) + 1;
    //                 } else {
    //                     // Process requires multiple items to build
    //                     $this->cost[$proc][$mat] = ($this->cost[$proc][$mat] ?? 0) + $cost;
    //                 }
    //             }
    //         }
    //
    //         // foreach ($materials as $class => $cost) {
    //         //     /** @var \Sevtech\Item\Item $material */
    //         //     $material = new $class();
    //         //     $name = $material->name();
    //         //
    //         //     if ($material instanceof Process\Process) {
    //         //         foreach ($material->cost() as ) {
    //         //
    //         //         }
    //         //     } else {
    //         //         $costs[$name] = ($costs[$name] ?? 0) + $cost;
    //         //     }
    //         // }
    //     }
    // }
}