<?php

namespace Sevtech\Age;

/**
 *
 */
class One extends Age
{

    /**
     * Print friendly age name.
     *
     * @var string
     */
    protected $name = 'One';

}