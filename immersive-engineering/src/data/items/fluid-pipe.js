import IronPlate from './iron-plate'

export default class FluidPipe {
  constructor () {
    this.name = 'Fluid Pipe'
    this.yields = 8
  }

  crafting () {
    return [
      IronPlate, IronPlate, IronPlate,
      null, null, null,
      IronPlate, IronPlate, IronPlate
    ]
  }
}
