<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class CrushedBlackQuartz extends Item implements Process\Process
{
    ///
    public function process()
    {
        return [
            Process\Grindstone::class => BlackQuartzOre::class,
        ];
    }
}