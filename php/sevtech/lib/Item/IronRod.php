<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class IronRod extends Item implements Process\Process
{
    ///
    public function process()
    {
        return [
            Process\MetalPress::ROD => [
                IronIngot::class => 2,
                IronPlate::class => 2,
            ],
            Process\StoneAnvil::class => IronPlate::class,
        ];
    }
}