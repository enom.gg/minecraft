import EngineersWireCutters from '../tools/engineers-wire-cutters'
import SteelPlate from './steel-plate'

export default class SteelWire {
  constructor () {
    this.name = 'Steel Wire'
  }

  crafting () {
    return [
      SteelPlate, EngineersWireCutters
    ]
  }
}
