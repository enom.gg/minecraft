import HVWireConnector from './hv-wire-connector'
import IronIngot from './iron-ingot'
import RedstoneDust from './redstone-dust'
import RedstoneRepeater from './redstone-repeater'

export default class RedstoneBreaker {
  constructor () {
    this.name = 'Redstone Breaker'
  }

  crafting () {
    return [
      HVWireConnector, null, HVWireConnector,
      IronIngot, RedstoneRepeater, IronIngot,
      IronIngot, RedstoneDust, IronIngot
    ]
  }
}
