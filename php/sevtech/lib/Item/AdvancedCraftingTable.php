<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;
use Sevtech\Age;

/**
 *
 */
class AdvancedCraftingTable extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Age\Three::class => [
                Crafting::class => [
                    AdvancedComponent::class => 2,
                    AdvancedCatalyst::class => 2,
                    BasicCraftingTable::class => 2,
                    BlockOfGold::class => 2,
                    BlackIronSlate::class => 1,
                ],
            ],
        ];
    }
}