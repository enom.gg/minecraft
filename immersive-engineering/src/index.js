import { render } from 'solid-js/web'

import './app.sass'

import App from './app'
import DataProvider from './data'
import RouterProvider from './router'

render(() => (
  <RouterProvider>
    <DataProvider>
      <App />
    </DataProvider>
  </RouterProvider>
), document.getElementById('root'))
