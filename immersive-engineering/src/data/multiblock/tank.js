import { wants } from '--/utils'
import EngineersHammer from '../tools/engineers-hammer'
import IronSheetmetal from '../items/iron-sheetmetal'
import TreatedWoodFence from '../items/treated-wood-fence'

export default class Tank {
  constructor () {
    this.multi = true
    this.name = 'Tank'
  }

  crafting () {
    return [
      EngineersHammer,
      wants(4, TreatedWoodFence),
      wants(34, IronSheetmetal)
    ].flat()
  }
}
