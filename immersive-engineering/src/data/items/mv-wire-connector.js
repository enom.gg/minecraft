import ElectrumIngot from './electrum-ingot'
import Terracotta from './terracotta'

export default class MVWireConnector {
  constructor () {
    this.name = 'MV Wire Connector'
    this.yields = 4
  }

  crafting () {
    return [
      null, ElectrumIngot, null,
      Terracotta, ElectrumIngot, Terracotta,
      Terracotta, ElectrumIngot, Terracotta
    ]
  }
}
