import IronIngot from './iron-ingot'

export default class Cauldron {
  constructor () {
    this.name = 'Cauldron'
  }

  crafting () {
    return [
      IronIngot, null, IronIngot,
      IronIngot, null, IronIngot,
      IronIngot, IronIngot, IronIngot
    ]
  }
}
