import MVWireCoil from './mv-wire-coil'
import ToughFabric from './tough-fabric'

export default class InsulatedMVWireCoil {
  constructor () {
    this.name = 'Insulated MV Wire Coil'
    this.yields = 4
  }

  crafting () {
    return [
      ToughFabric, MVWireCoil, ToughFabric,
      MVWireCoil, ToughFabric, MVWireCoil,
      ToughFabric, MVWireCoil, ToughFabric
    ]
  }
}
