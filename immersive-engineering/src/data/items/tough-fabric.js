import IndustrialHempFiber from './industrial-hemp-fiber'
import Stick from './stick'

export default class ToughFabric {
  constructor () {
    this.name = 'Tough Fabric'
  }

  crafting () {
    return [
      IndustrialHempFiber, IndustrialHempFiber, IndustrialHempFiber,
      IndustrialHempFiber, Stick, IndustrialHempFiber,
      IndustrialHempFiber, IndustrialHempFiber, IndustrialHempFiber
    ]
  }
}
