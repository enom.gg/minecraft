import RedstoneDust from './redstone-dust'
import Stick from './stick'

export default class RedstoneTorch {
  constructor () {
    this.name = 'Redstone Torch'
  }

  crafting () {
    return [
      null, RedstoneDust,
      null, Stick
    ]
  }
}
