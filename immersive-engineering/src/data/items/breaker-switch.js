import CopperIngot from './copper-ingot'
import Lever from './lever'
import Terracotta from './terracotta'

export default class BreakerSwitch {
  constructor () {
    this.name = 'Breaker Switch'
  }

  crafting () {
    return [
      null, null, null,
      null, Lever, null,
      Terracotta, CopperIngot, Terracotta
    ]
  }
}
