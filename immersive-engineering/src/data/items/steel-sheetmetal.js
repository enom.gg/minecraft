import SteelPlate from './steel-plate'

export default class SteelSheetmetal {
  constructor () {
    this.name = 'Steel Sheetmetal'
    this.yields = 4
  }

  crafting () {
    return [
      null, SteelPlate, null,
      SteelPlate, null, SteelPlate,
      null, SteelPlate, null
    ]
  }
}
