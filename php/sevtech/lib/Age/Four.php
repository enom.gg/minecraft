<?php

namespace Sevtech\Age;

/**
 *
 */
class Four extends Age
{

    /**
     * Print friendly age name.
     *
     * @var string
     */
    protected $name = 'Four';

}