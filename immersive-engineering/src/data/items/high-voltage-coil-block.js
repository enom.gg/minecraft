import HVWireCoil from './hv-wire-coil'
import IronIngot from './iron-ingot'

export default class HighVoltageCoilBlock {
  constructor () {
    this.name = 'High-Voltage Coil Block'
  }

  crafting () {
    return [
      HVWireCoil, HVWireCoil, HVWireCoil,
      HVWireCoil, IronIngot, HVWireCoil,
      HVWireCoil, HVWireCoil, HVWireCoil
    ]
  }
}
