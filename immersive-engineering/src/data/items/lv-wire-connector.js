import CopperIngot from './copper-ingot'
import Terracotta from './terracotta'

export default class LVWireConnector {
  constructor () {
    this.name = 'LV Wire Connector'
    this.yields = 4
  }

  crafting () {
    return [
      null, CopperIngot, null,
      Terracotta, CopperIngot, Terracotta,
      Terracotta, CopperIngot, Terracotta
    ]
  }
}
