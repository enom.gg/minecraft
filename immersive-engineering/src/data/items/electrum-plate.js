import ElectrumIngot from './electrum-ingot'
import EngineersHammer from '../tools/engineers-hammer'

export default class ElectrumPlate {
  constructor () {
    this.name = 'Electrum Plate'
  }

  crafting () {
    return [
      ElectrumIngot, EngineersHammer
    ]
  }
}
