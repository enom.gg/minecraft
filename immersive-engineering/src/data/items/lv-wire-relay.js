import CopperIngot from './copper-ingot'
import Terracotta from './terracotta'

export default class LVWireRelay {
  constructor () {
    this.name = 'LV Wire Relay'
    this.yields = 8
  }

  crafting () {
    return [
      null, null, null,
      null, CopperIngot, null,
      Terracotta, CopperIngot, Terracotta
    ]
  }
}
