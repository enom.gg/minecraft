import ConveyorBelt from './conveyor-belt'
import RedstoneTorch from './redstone-torch'

export default class RedstoneControllerConveyorBelt {
  constructor () {
    this.name = 'Redstone Controller Conveyor Belt'
  }

  crafting () {
    return [
      null, ConveyorBelt,
      null, RedstoneTorch
    ]
  }
}
