import CopperIngot from './copper-ingot'
import EngineersHammer from '../tools/engineers-hammer'

export default class CopperPlate {
  constructor () {
    this.name = 'Copper Plate'
  }

  crafting () {
    return [
      CopperIngot, EngineersHammer
    ]
  }
}
