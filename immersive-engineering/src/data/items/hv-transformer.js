import HVWireConnector from './hv-wire-connector'
import IronIngot from './iron-ingot'
import MVWireConnector from './mv-wire-connector'
import HighVoltageCoilBlock from './high-voltage-coil-block'

export default class HVTransformer {
  constructor () {
    this.name = 'HV Transformer'
  }

  crafting () {
    return [
      MVWireConnector, null, HVWireConnector,
      IronIngot, HighVoltageCoilBlock, IronIngot,
      IronIngot, IronIngot, IronIngot
    ]
  }
}
