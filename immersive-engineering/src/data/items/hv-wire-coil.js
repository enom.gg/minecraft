import AluminiumWire from './aluminium-wire'
import SteelWire from './steel-wire'
import Stick from './stick'

export default class HVWireCoil {
  constructor () {
    this.name = 'HV Wire Coil'
    this.yields = 4
  }

  crafting () {
    return [
      null, SteelWire, null,
      AluminiumWire, Stick, AluminiumWire,
      null, SteelWire, null
    ]
  }
}
