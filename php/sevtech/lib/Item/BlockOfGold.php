<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class BlockOfGold extends Item implements Process\Process
{

    /**
     * Print friendly class name.
     *
     * @var string
     */
    public $name = 'Block of Gold';

    /**
     * Returns the list of processes that creates the item.
     *
     * @return array
     */
    public function process()
    {
        return [
            Process\Casting::BASIN => [
                GoldIngot::class => 9,
            ],
            Process\MetalPress::PACKING_3X3 => [
                GoldIngot::class => 9,
            ],
        ];
    }
}