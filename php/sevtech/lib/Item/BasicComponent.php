<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;
use Sevtech\Age;

/**
 *
 */
class BasicComponent extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Age\Two::class => [
                Crafting::class => [
                    IronRod::class => 2,
                    BlackIronSlate::class => 1,
                    StarDust::class => 1,
                ],
            ],
            Age\Three::class => [
                Crafting::class => [
                    IronRod::class => 2,
                    BlackIronSlate::class => 1,
                    Luminessence::class => 1,
                ],
            ]
        ];
    }

}