import IronIngot from './iron-ingot'

export default class IronTrapdoor {
  constructor () {
    this.name = 'Iron Trapdoor'
  }

  crafting () {
    return [
      IronIngot, IronIngot,
      IronIngot, IronIngot
    ]
  }
}
