import AluminiumIngot from './aluminium-ingot'
import EngineersHammer from '../tools/engineers-hammer'

export default class AluminiumPlate {
  constructor () {
    this.name = 'Aluminium Plate'
  }

  crafting () {
    return [
      AluminiumIngot, EngineersHammer
    ]
  }
}
