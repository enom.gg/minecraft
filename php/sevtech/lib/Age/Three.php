<?php

namespace Sevtech\Age;

/**
 *
 */
class Three extends Age
{

    /**
     * Print friendly age name.
     *
     * @var string
     */
    protected $name = 'Three';

}