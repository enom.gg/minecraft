import Brick from './brick'
import MagmaBlock from './magma-block'
import NetherBrick from './nether-brick'

export default class BlastBrick {
  constructor () {
    this.name = 'Blast Brick'
    this.yields = 3
  }

  crafting () {
    return [
      NetherBrick, Brick, NetherBrick,
      Brick, MagmaBlock, Brick,
      NetherBrick, Brick, NetherBrick
    ]
  }
}
