<style>
    div.block {
        width: 300px;
        margin: 5px 5px 25px 25px;
        /*float: left;*/
    }

    div.sub {
        margin-left: 25px;
    }

    .recurse {
        display: block; 
        padding: 5px 5px 5px 15px; 
        border-left: 10px solid #ccc;
        border-bottom: 1px solid #ccc;
        color: #069; 
        text-decoration: none;
    }

    .recurse:hover {
        background-color: #ddd;
    }

    a.mats {

    }

    a.out {
        color: #069;
    }

</style>
<script src="jquery-1.7.2.min.js" type="application/javascript"></script>
<script type="application/javascript">

    $(function () {
        $("a.out").attr("target", "_blank");

        $("a.expand").toggle(function (e) {
            e.preventDefault();

            $(this).text("(hide)")
                .parent()
                .siblings()
                .show();
        }, function (e) {
            e.preventDefault();

            $(this).text("(show)")
                .parent()
                .siblings()
                .hide();
        });

        $("a.expand").parent()
            .siblings()
            .hide();
    });

</script>

<?php
include("basic.set.php");
include("item.set.php");
?>

<div style="float: left; width: 25%; margin: 2%; border: 1px solid #ddd; padding: 2%;">

<?php
foreach (get_declared_classes() as $class) {
    if (get_parent_class($class) == "Item") {
        ?>
            <a class="recurse" href="?item=<?php echo $class; ?>"><?php echo implode(" ", explode("_", $class)); ?></a>
        <?php
        } // end get parent class
    } // end foreach class
    ?>
    <p style="margin: 25px 15px 0; color: #0399dd; font-size: 1.2em;">Want something added? Just ask Enom!</p>
</div>

<?php
if (isset($_GET['item'])) {
    if (!class_exists($_GET['item'])) {
        ?>
        <h1><?php echo $_GET['item']; ?> doesn't exist (yet?)</h1>
    <?php
    } else {

        if(isset($_GET['qty'])){
            $qty = $_GET['qty'];
            $item = $_GET['item'];
            
            $stack = new Item;
            $stack->requires = array(
                array($qty, $item),
            );
            
            var_dump($stack->recipe());
            
        }
        
        
        $item = new $_GET['item'];
        $link = $item->webLink();
        $mats = $item->recipe();
        $tree = $item->buildTree();

        function recurse($data, $top = true, $mul = 1, $stk = 1) {
            foreach ($data as $item => $qty) {
                ?>
                <div class="<?php echo $top ? "block" : "sub"; ?>">
                    <span class="recurse" href="<?php echo $item::model()->webLink(); ?>"><a class="out" href="<?php echo $item::model()->webLink(); ?>"><?php echo $item::model()->name(true); ?></a>:
                <?php if (is_array($qty)) { ?>
                            <span style='font-weight: bold; font-size: 1.2em; color: #666;'><?php echo $qty[0]; ?></span>
                            <a class="expand" href="#" style="float: right; color: #390;">(show)</a>
                        </span>
                        <?php recurse($qty[1], false, $qty[0], $item::model()->stack); ?>
                <?php } else { ?>
                        <span style='font-weight: bold; font-size: 1.2em; color: #666;'><?php echo ($qty * ceil($mul / $stk)); ?></span>
                    </span>
                <?php } // end if qty array  ?>
                </div>
            <?php
            } // end foreach
        }

// end recurse
        ?>
        <div style="float: left; width: 65%;">
            <h2 style="margin: 40px 40px 0px;">Building <a class="out" href="<?php echo $link; ?>"><?php echo $item->name(true); ?></a></h2>
            <div style="float: left; width: 40%; margin: 2%; border: 1px solid #ddd; padding: 2%;">
                <h4>Total Materials Required</h4>
                <?php foreach ($mats as $item => $qty) { ?>
                    <a class="recurse out" href="<?php echo $item::model()->webLink(); ?>" style="width: 175px; text-decoration: underline;">
                        <span style="display: inline-block; width: 25px; font-weight: bold; color: #069; text-align: right; padding-right: 25px;"><?php echo $qty; ?></span>
                        <span><?php echo $item::model()->name(true); ?></span>
                    </a>
                <?php } // end foreach materials  ?>
            </div>
            <div style="float: left; width: 40%; margin: 2%; border: 1px solid #ddd; padding: 2%;">
                <h4>Build Tree</h4>
                <?php recurse($tree); ?>
            </div>
        </div>

    <?php
    } // end class exists
} // end get
?>

