import EngineersHammer from '../tools/engineers-hammer'
import NickelIngot from './nickel-ingot'

export default class NickelPlate {
  constructor () {
    this.name = 'Nickel Plate'
  }

  crafting () {
    return [
      NickelIngot, EngineersHammer
    ]
  }
}
