import { createState } from 'solid-js'
import { For } from 'solid-js/dom'

import Crafting from './crafting'
import { use as data } from '--/data/crafting'

export default function Tree () {
  const [{ state }] = data()
  const [expanded, setExpanded] = createState()
  const expand = (name, toggle = true) => {
    if (toggle) return setExpanded(name, (v) => !v)
    return expanded[name]
  }
  const tree = () => state.crafting.sort((a, b) => a.name > b.name)

  return (
    <>
      <h3 class='py-2 border-t mt-4 text-xl font-bold text-gray-600'>Build Tree</h3>

      <div class='mb-2 grid grid-cols-12 py-2'>
        <For
          each={tree()}
          fallback={(
            <div class='col-start-2 col-span-11 text-gray-400'>Nothing selected</div>
          )}
        >
          {(item) => (
            <Branch
              expand={[expand]}
              name={item.name}
              quantity={item.quantity}
            />
          )}
        </For>
      </div>
    </>
  )
}

export function Branch (props) {
  const { expand: [expand, parent = ''] } = props
  const [, { add, named, newed, remove }] = data()
  const item = () => named(props.name)
  const crafting = () => item().crafting().reduce((materials, constructor) => {
    if (!constructor) return materials

    const material = newed(constructor)
    const existing = materials.find((item) => item.name === material.name)

    if (existing) {
      if (!material.tool) existing.quantity++
    } else {
      materials.push({ name: material.name, quantity: 1 })
    }

    return materials
  }, [])
  const expanded = () => expand(props.name + parent, false)
  const quantify = (material) => {
    if (named(material.name).tool) return 1
    return material.quantity * Math.ceil(props.quantity / (item().yields || 1))
  }

  return (
    <>
      <div class='text-center border-t py-2'>
        <button onClick={[remove, props.name]} class='px-3 py-1 border rounded'>-</button>
      </div>

      <div class='text-center border-t py-2'>
        {props.quantity}
      </div>

      <div class='text-center border-t py-2'>
        <button onClick={[add, props.name]} class='px-3 py-1 border rounded'>+</button>
      </div>

      <div class='col-span-9 border-t py-2'>
        {item().crafting ? (
          <button class='w-full h-full text-left' onClick={[expand, props.name + parent]}>
            {props.name}

            <span class='float-right px-3'>
              {expanded() ? String.fromCharCode(8743) : String.fromCharCode(8744)}
            </span>
          </button>
        ) : (
          <span class=''>{props.name}</span>
        )}
      </div>

      {expanded() && (
        <>
          <div class='col-span-3'>
            <Crafting name={props.name} />
          </div>

          <div class='col-span-9 self-end mb-2 ml-4'>
            {item().multi && (
              <p class='p-1 italic'>Multiblock structure</p>
            )}

            {item().tool && (
              <p class='p-1 italic'>Tool</p>
            )}

            {item().yields && (
              <p class='p-1'>Yields {item().yields} items</p>
            )}

            <p class='p-1 font-semibold'>Materials required:</p>
          </div>

          <For each={crafting()}>
            {(material) => (
              <div class='col-start-2 col-span-11'>
                <div class='grid grid-cols-12'>
                  <Branch
                    expand={[expand, props.name + parent]}
                    name={material.name}
                    quantity={quantify(material)}
                  />
                </div>
              </div>
            )}
          </For>
        </>
      )}
    </>
  )
}
