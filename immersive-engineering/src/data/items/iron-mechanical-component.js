import CopperIngot from './copper-ingot'
import IronPlate from './iron-plate'

export default class IronMechanicalComponent {
  constructor () {
    this.name = 'Iron Mechanical Component'
  }

  crafting () {
    return [
      IronPlate, null, IronPlate,
      null, CopperIngot, null,
      IronPlate, null, IronPlate
    ]
  }
}
