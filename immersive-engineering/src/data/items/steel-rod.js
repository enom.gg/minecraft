import SteelIngot from './steel-ingot'

export default class SteelRod {
  constructor () {
    this.name = 'Steel Rod'
    this.yields = 4
  }

  crafting () {
    return [
      null, SteelIngot,
      null, SteelIngot
    ]
  }
}
