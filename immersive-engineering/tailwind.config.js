module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
    './src/**/*.sass'
  ],
  theme: {},
  variants: {},
  plugins: []
}
