import { wants } from '--/utils'
import BlockOfSteel from '../items/block-of-steel'
import Cauldron from '../items/cauldron'
import EngineersHammer from '../tools/engineers-hammer'
import HeavyEngineeringBlock from '../items/heavy-engineering-block'
import LightEngineeringBlock from '../items/light-engineering-block'
import RedstoneEngineeringBlock from '../items/redstone-engineering-block'
import ReinforcedBlastBrick from '../items/reinforced-blast-brick'
import SteelScaffolding from '../items/steel-scaffolding'
import SteelSheetmetal from '../items/steel-sheetmetal'
import SteelSheetmetalSlab from '../items/steel-sheetmetal-slab'

export default class ArcFurnace {
  constructor () {
    this.multi = true
    this.name = 'Arc Furnace'
  }

  crafting () {
    return [
      EngineersHammer,
      wants(8, SteelSheetmetal),
      wants(6, BlockOfSteel),
      wants(14, SteelSheetmetalSlab),
      wants(5, SteelScaffolding),
      wants(5, HeavyEngineeringBlock),
      wants(1, Cauldron),
      wants(1, RedstoneEngineeringBlock),
      wants(10, LightEngineeringBlock),
      wants(27, ReinforcedBlastBrick)
    ].flat()
  }
}
