<?php

namespace Sevtech\Process;

/**
 *
 */
interface Crafting extends Process
{

    /**
     * Callback for crafting in age 0.
     *
     * @var string
     */
    public const AGE_ONE = self::class . '.age1';

    /**
     * Callback for crafting in age 0.
     *
     * @var string
     */
    public const AGE_TWO = self::class . '.age2';

    /**
     * Callback for crafting in age 0.
     *
     * @var string
     */
    public const AGE_THREE = self::class . '.age3';

    /**
     * Callback for crafting in age 0.
     *
     * @var string
     */
    public const AGE_FOUR = self::class . '.age4';

    /**
     * Callback for crafting in age 0.
     *
     * @var string
     */
    public const AGE_FIVE = self::class . '.age5';

    /**
     * Materials key to specify recipe item yield amount.
     *
     * @var string
     */
    public const YIELDS = self::class . '.yields';

}