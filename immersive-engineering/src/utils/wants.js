export default function wants (quantity, of) {
  return Array(quantity).fill(of)
}
