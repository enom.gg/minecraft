<?php

namespace Sevtech\Age;

/**
 *
 */
class Five extends Age
{

    /**
     * Print friendly age name.
     *
     * @var string
     */
    protected $name = 'Five';

}