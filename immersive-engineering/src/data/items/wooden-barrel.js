import TreatedWoodPlanks from './treated-wood-planks'
import TreatedWoodSlab from './treated-wood-slab'

export default class WoodenBarrel {
  constructor () {
    this.name = 'Wooden Barrel'
  }

  crafting () {
    return [
      TreatedWoodSlab, TreatedWoodSlab, TreatedWoodSlab,
      TreatedWoodPlanks, null, TreatedWoodPlanks,
      TreatedWoodPlanks, TreatedWoodPlanks, TreatedWoodPlanks
    ]
  }
}
