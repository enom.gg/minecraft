import IronIngot from './iron-ingot'
import LVWireCoil from './lv-wire-coil'

export default class CopperCoilBlock {
  constructor () {
    this.name = 'Copper Coil Block'
  }

  crafting () {
    return [
      LVWireCoil, LVWireCoil, LVWireCoil,
      LVWireCoil, IronIngot, LVWireCoil,
      LVWireCoil, LVWireCoil, LVWireCoil
    ]
  }
}
