<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;
use Sevtech\Age;

/**
 *
 */
class AdvancedCatalyst extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Age\Three::class => [
                Crafting::class => [
                    AdvancedComponent::class => 4,
                    BlackIronIngot::class => 1,
                ],
            ],
        ];
    }
}