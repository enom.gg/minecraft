import { wants } from '--/utils'
import EngineersHammer from '../tools/engineers-hammer'
import FluidPipe from '../items/fluid-pipe'
import LightEngineeringBlock from '../items/light-engineering-block'
import Piston from '../items/piston'
import RedstoneEngineeringBlock from '../items/redstone-engineering-block'
import SteelFence from '../items/steel-fence'
import SteelScaffolding from '../items/steel-scaffolding'
import WoodenBarrel from '../items/wooden-barrel'

export default class Squeezer {
  constructor () {
    this.multi = true
    this.name = 'Squeezer'
  }

  crafting () {
    return [
      EngineersHammer,
      wants(1, Piston),
      wants(6, SteelScaffolding),
      wants(2, LightEngineeringBlock),
      wants(1, RedstoneEngineeringBlock),
      wants(3, SteelFence),
      wants(2, FluidPipe),
      wants(4, WoodenBarrel)
    ].flat()
  }
}
