import Glass from './glass'
import GreenDye from './green-dye'
import IronGrit from './iron-grit'

export default class InsulatingGlass {
  constructor () {
    this.name = 'Insulating Glass'
    this.yields = 2
  }

  crafting () {
    return [
      null, Glass, null,
      IronGrit, GreenDye, IronGrit,
      null, Glass, null
    ]
  }
}
