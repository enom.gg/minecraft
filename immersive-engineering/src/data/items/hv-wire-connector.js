import AluminiumIngot from './aluminium-ingot'
import Terracotta from './terracotta'

export default class HVWireConnector {
  constructor () {
    this.name = 'HV Wire Connector'
    this.yields = 4
  }

  crafting () {
    return [
      null, AluminiumIngot, null,
      Terracotta, AluminiumIngot, Terracotta,
      Terracotta, AluminiumIngot, Terracotta
    ]
  }
}
