import { wants } from '--/utils'
import Cauldron from '../items/cauldron'
import EngineersHammer from '../tools/engineers-hammer'
import FluidPipe from '../items/fluid-pipe'
import IronSheetmetal from '../items/iron-sheetmetal'
import LightEngineeringBlock from '../items/light-engineering-block'
import RedstoneEngineeringBlock from '../items/redstone-engineering-block'
import SteelScaffolding from '../items/steel-scaffolding'

export default class Fermenter {
  constructor () {
    this.multi = true
    this.name = 'Fermenter'
  }

  crafting () {
    return [
      EngineersHammer,
      wants(6, SteelScaffolding),
      wants(2, LightEngineeringBlock),
      wants(4, Cauldron),
      wants(1, RedstoneEngineeringBlock),
      wants(4, IronSheetmetal),
      wants(2, FluidPipe)
    ].flat()
  }
}
