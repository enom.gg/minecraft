import { For } from 'solid-js/dom'

import { use as data } from '--/data/crafting'

export default function Tools () {
  const [{ state }, { named }] = data()
  const tools = () => state.materials
    .filter((material) => named(material.name).tool)
    .sort((a, b) => a.name > b.name)

  return (
    <>
      <h3 class='mt-4 py-2 border-t text-xl font-bold text-gray-600'>Tools</h3>

      <div class='grid grid-cols-12 gap-1'>
        <For
          each={tools()}
          fallback={(
            <div class='col-start-2 col-span-11 text-gray-400'>Nothing required</div>
          )}
        >
          {(item) => (
            <div class='col-start-4 col-span-9'>{item.name}</div>
          )}
        </For>
      </div>
    </>
  )
}
