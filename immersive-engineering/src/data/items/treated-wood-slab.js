import TreatedWoodPlanks from './treated-wood-planks'

export default class TreatedWoodSlab {
  constructor () {
    this.name = 'Treated Wood Slab'
    this.yields = 6
  }

  crafting () {
    return [
      null, null, null,
      TreatedWoodPlanks, TreatedWoodPlanks, TreatedWoodPlanks,
      null, null, null
    ]
  }
}
