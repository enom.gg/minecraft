import IronIngot from './iron-ingot'
import MVWireCoil from './mv-wire-coil'

export default class ElectrumCoilBlock {
  constructor () {
    this.name = 'Electrum Coil Block'
  }

  crafting () {
    return [
      MVWireCoil, MVWireCoil, MVWireCoil,
      MVWireCoil, IronIngot, MVWireCoil,
      MVWireCoil, MVWireCoil, MVWireCoil
    ]
  }
}
