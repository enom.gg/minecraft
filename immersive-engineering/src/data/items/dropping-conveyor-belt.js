import ConveyorBelt from './conveyor-belt'
import IronTrapdoor from './iron-trapdoor'

export default class DroppingConveyorBelt {
  constructor () {
    this.name = 'Dropping Conveyor Belt'
  }

  crafting () {
    return [
      null, ConveyorBelt,
      null, IronTrapdoor
    ]
  }
}
