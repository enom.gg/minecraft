import IronIngot from './iron-ingot'

export default class BlockOfIron {
  constructor () {
    this.name = 'Block of Iron'
  }

  crafting () {
    return [
      IronIngot, IronIngot, IronIngot,
      IronIngot, IronIngot, IronIngot,
      IronIngot, IronIngot, IronIngot
    ]
  }
}
