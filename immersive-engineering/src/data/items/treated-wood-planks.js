import BucketOfCreosoteOil from './bucket-of-creosote-oil'
import OakPlanks from './oak-planks'

export default class TreatedWoodPlanks {
  constructor () {
    this.name = 'Treated Wood Planks'
    this.yields = 8
  }

  crafting () {
    return [
      OakPlanks, OakPlanks, OakPlanks,
      OakPlanks, BucketOfCreosoteOil, OakPlanks,
      OakPlanks, OakPlanks, OakPlanks
    ]
  }
}
