import LeadIngot from './lead-ingot'

export default class BlockOfLead {
  constructor () {
    this.name = 'Block of Lead'
  }

  crafting () {
    return [
      LeadIngot, LeadIngot, LeadIngot,
      LeadIngot, LeadIngot, LeadIngot,
      LeadIngot, LeadIngot, LeadIngot
    ]
  }
}
