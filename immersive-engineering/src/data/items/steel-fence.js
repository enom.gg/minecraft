import SteelIngot from './steel-ingot'
import SteelRod from './steel-rod'

export default class SteelFence {
  constructor () {
    this.name = 'Steel Fence'
    this.yields = 3
  }

  crafting () {
    return [
      null, null, null,
      SteelIngot, SteelRod, SteelIngot,
      SteelIngot, SteelRod, SteelIngot
    ]
  }
}
