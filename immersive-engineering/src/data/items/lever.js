import Cobblestone from './cobblestone'
import Stick from './stick'

export default class Lever {
  constructor () {
    this.name = 'Lever'
  }

  crafting () {
    return [
      null, Stick,
      null, Cobblestone
    ]
  }
}
