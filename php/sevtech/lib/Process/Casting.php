<?php

namespace Sevtech\Process;

/**
 *
 */
interface Casting extends Process
{

    /**
     * Callback for using a basin instead of a cast.
     *
     * @var string
     */
    public const BASIN = self::class . '.basin';

    /**
     * Callback for casting a plate.
     *
     * @var string
     */
    public const PLATE = self::class . '.plate';
}