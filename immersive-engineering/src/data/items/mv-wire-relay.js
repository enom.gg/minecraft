import ElectrumIngot from './electrum-ingot'
import Terracotta from './terracotta'

export default class MVWireRelay {
  constructor () {
    this.name = 'MV Wire Relay'
    this.yields = 8
  }

  crafting () {
    return [
      null, null, null,
      null, ElectrumIngot, null,
      Terracotta, ElectrumIngot, Terracotta
    ]
  }
}
