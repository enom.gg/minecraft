import { wants } from '--/utils'
import EngineersHammer from '../tools/engineers-hammer'
import FluidPipe from '../items/fluid-pipe'
import GeneratorBlock from '../items/generator-block'
import HeavyEngineeringBlock from '../items/heavy-engineering-block'
import RadiatorBlock from '../items/radiator-block'
import RedstoneEngineeringBlock from '../items/redstone-engineering-block'
import SteelScaffolding from '../items/steel-scaffolding'

export default class DieselGenerator {
  constructor () {
    this.multi = true
    this.name = 'Diesel Generator'
  }

  crafting () {
    return [
      EngineersHammer,
      wants(9, RadiatorBlock),
      wants(6, SteelScaffolding),
      wants(4, GeneratorBlock),
      wants(13, HeavyEngineeringBlock),
      wants(1, RedstoneEngineeringBlock),
      wants(5, FluidPipe)
    ].flat()
  }
}
