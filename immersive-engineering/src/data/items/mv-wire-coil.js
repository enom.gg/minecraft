import ElectrumWire from './electrum-wire'
import Stick from './stick'

export default class MVWireCoil {
  constructor () {
    this.name = 'MV Wire Coil'
    this.yields = 4
  }

  crafting () {
    return [
      null, ElectrumWire, null,
      ElectrumWire, Stick, ElectrumWire,
      null, ElectrumWire, null
    ]
  }
}
