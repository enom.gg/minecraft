<?php

namespace Sevtech\Process;

/**
 *
 */
interface MetalPress extends Process
{

    /**
     * Process key for specifying the mold to use.
     *
     * @var string
     */
    public const MOLD = 'Mold';

    /**
     * Callback for requiring the packing 2x2 mold.
     *
     * @var string
     */
    public const PACKING_2X2 = 'Packing 2x2';

    /**
     * Callback for requiring the packing 3x3 mold.
     *
     * @var string
     */
    public const PACKING_3X3 = 'Packing 3x3';

    /**
     * Callback for requiring the plate mold.
     *
     * @var string
     */
    public const PLATE = 'Plate';

    /**
     * Callback for requiring the rod mold.
     *
     * @var string
     */
    public const ROD = 'Rod';

    /**
     * Callback for requiring the unpacking mold.
     *
     * @var string
     */
    public const UNPACKING = 'Unpacking';

}