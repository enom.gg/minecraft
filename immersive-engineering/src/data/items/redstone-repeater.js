import RedstoneDust from './redstone-dust'
import RedstoneTorch from './redstone-torch'
import Stone from './stone'

export default class RedstoneRepeater {
  constructor () {
    this.name = 'Redstone Repeater'
  }

  crafting () {
    return [
      null, null, null,
      RedstoneTorch, RedstoneDust, RedstoneTorch,
      Stone, Stone, Stone
    ]
  }
}
