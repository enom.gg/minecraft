import TreatedStick from './treated-stick'
import TreatedWoodPlanks from './treated-wood-planks'

export default class TreatedWoodFence {
  constructor () {
    this.name = 'Treated Wood Fence'
    this.yields = 3
  }

  crafting () {
    return [
      null, null, null,
      TreatedWoodPlanks, TreatedStick, TreatedWoodPlanks,
      TreatedWoodPlanks, TreatedStick, TreatedWoodPlanks
    ]
  }
}
