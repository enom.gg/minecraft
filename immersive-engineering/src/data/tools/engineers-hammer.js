import IronIngot from '../items/iron-ingot'
import String from '../items/string'
import Stick from '../items/stick'

export default class EngineersHammer {
  constructor () {
    this.name = 'Engineer\'s Hammer'
    this.tool = true
  }

  crafting () {
    return [
      null, IronIngot, String,
      null, Stick, IronIngot,
      Stick, null, null
    ]
  }
}
