import { CraftingProvider } from './crafting'

export default function Provider (props) {
  return (
    <CraftingProvider>
      {props.children}
    </CraftingProvider>
  )
}
