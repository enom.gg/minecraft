import ElectrumPlate from './electrum-plate'
import KineticDynamo from './kinetic-dynamo'
import SteelSheetmetal from './steel-sheetmetal'

export default class GeneratorBlock {
  constructor () {
    this.name = 'Generator Block'
    this.yields = 4
  }

  crafting () {
    return [
      SteelSheetmetal, ElectrumPlate, SteelSheetmetal,
      ElectrumPlate, KineticDynamo, ElectrumPlate,
      SteelSheetmetal, ElectrumPlate, SteelSheetmetal
    ]
  }
}
