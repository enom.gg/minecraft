import Glass from './glass'
import IronMechanicalComponent from './iron-mechanical-component'
import TreatedWoodPlanks from './treated-wood-planks'
import VacuumTube from './vacuum-tube'

export default class GardenCloche {
  constructor () {
    this.name = 'Garden Cloche'
  }

  crafting () {
    return [
      Glass, VacuumTube, Glass,
      Glass, null, Glass,
      TreatedWoodPlanks, IronMechanicalComponent, TreatedWoodPlanks
    ]
  }
}
