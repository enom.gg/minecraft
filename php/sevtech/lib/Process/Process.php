<?php

namespace Sevtech\Process;

/**
 *
 */
interface Process
{
    public function process();
}