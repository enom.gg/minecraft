import { createState } from 'solid-js'
import { For } from 'solid-js/dom'

import { use as data } from '--/data/crafting'

export default function Search () {
  const [getState, setState] = createState({ term: '' })
  const [{ state, items, multi, tools }, { add, remove, update }] = data()
  const search = (item) => getState.term ? new RegExp(getState.term, 'i').test(item.name) : true
  const results = () => [multi.filter(search), tools.filter(search), items.filter(search)]
  const fetch = (name) => state.crafting.find((item) => item.name === name) || {}

  return (
    <div class='grid grid-cols-12 gap-1 mt-1'>
      <div class='text-right'>
        <button
          class='px-3 py-1 mt-px border rounded'
          onClick={() => setState('term', '')}
        > x
        </button>
      </div>

      <div class='col-span-11'>
        <input
          class='border-2 border-blue-300 px-3 py-1 rounded w-full'
          type='text'
          value={getState.term}
          onKeyUp={(e) => setState('term', e.target.value)}
        />
      </div>

      <For each={results()}>
        {(collection, i) => (
          <>
            {i() && (
              <hr class='col-span-full' />
            )}

            <For
              each={collection}
              fallback={(
                <div class='col-start-2 col-span-11 text-gray-400'>No results</div>
              )}
            >
              {({ name }) => (
                <>
                  <div class='text-right'>
                    <button
                      class='px-3 py-1 mt-px border rounded'
                      onClick={[remove, name]}
                    > -
                    </button>
                  </div>

                  <div class='text-center col-span-2'>
                    <input
                      class='p-1 w-full border-2 border-blue-300 rounded text-center'
                      type='number'
                      value={fetch(name).quantity || 0}
                      onKeyUp={(e) => update(name, e.target.value)}
                    />
                  </div>

                  <div class='text-left'>
                    <button
                      class='px-3 py-1 mt-px border rounded'
                      onClick={[add, name]}
                    > +
                    </button>
                  </div>

                  <div class='col-span-8'>{name}</div>
                </>
              )}
            </For>
          </>
        )}
      </For>
    </div>
  )
}
