import Materials from './components/materials'
import Search from './components/search'
import Tools from './components/tools'
import Tree from './components/tree'

export default function App () {
  return (
    <div class='grid grid-cols-2'>
      <div class='m-1 pl-2'>
        <Search />
      </div>

      <div class='border-l pl-2 m-1'>
        <Materials />

        <Tools />

        <Tree />
      </div>
    </div>
  )
}
