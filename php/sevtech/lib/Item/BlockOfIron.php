<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class BlockOfIron extends Item implements Process\Process
{

    /**
     * Print friendly class name.
     *
     * @var string
     */
    public $name = 'Block of Iron';

    /**
     * Returns the list of processes that creates the item.
     *
     * @return array
     */
    public function process()
    {
        return [
            Process\Casting::BASIN => [
                IronIngot::class => 9,
            ],
            Process\MetalPress::PACKING_3X3 => [
                IronIngot::class => 9,
            ],
        ];
    }
}