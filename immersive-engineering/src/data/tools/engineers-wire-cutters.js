import IronIngot from '../items/iron-ingot'
import Stick from '../items/stick'

export default class EngineersWireCutters {
  constructor () {
    this.name = 'Engineer\'s Wire Cutters'
    this.tool = true
  }

  crafting () {
    return [
      Stick, IronIngot,
      null, Stick
    ]
  }
}
