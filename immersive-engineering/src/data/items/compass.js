import IronIngot from './iron-ingot'
import RedstoneDust from './redstone-dust'

export default class Compass {
  constructor () {
    this.name = 'Compass'
  }

  crafting () {
    return [
      null, IronIngot, null,
      IronIngot, RedstoneDust, IronIngot,
      null, IronIngot, null
    ]
  }
}
