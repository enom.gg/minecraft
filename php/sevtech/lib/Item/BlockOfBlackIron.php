<?php

namespace Sevtech\Item;

use Sevtech\Process\MetalPress;

/**
 *
 */
class BlockOfBlackIron extends Item implements MetalPress
{

    /**
     * Print friendly class name.
     *
     * @var string
     */
    public $name = 'Block of Black Iron';

    /**
     * Returns the list of processes that creates the item.
     *
     * @return array
     */
    public function process()
    {
        return [
            MetalPress::PACKING_3X3 => [
                BlackIronIngot::class => 9,
            ],
        ];
    }
}