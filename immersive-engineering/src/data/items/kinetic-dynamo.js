import CopperCoilBlock from './copper-coil-block'
import IronIngot from './iron-ingot'
import RedstoneDust from './redstone-dust'

export default class KineticDynamo {
  constructor () {
    this.name = 'Kinetic Dynamo'
  }

  crafting () {
    return [
      null, null, null,
      RedstoneDust, CopperCoilBlock, RedstoneDust,
      IronIngot, IronIngot, IronIngot
    ]
  }
}
