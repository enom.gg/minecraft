import CopperWire from './copper-wire'
import Glass from './glass'
import NickelPlate from './nickel-plate'
import RedstoneDust from './redstone-dust'

export default class VacuumTube {
  constructor () {
    this.name = 'Vacuum Tube'
    this.yields = 3
  }

  crafting () {
    return [
      Glass, NickelPlate,
      CopperWire, RedstoneDust
    ]
  }
}
