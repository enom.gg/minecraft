import CopperWire from './copper-wire'
import Stick from './stick'

export default class LVWireCoil {
  constructor () {
    this.name = 'LV Wire Coil'
    this.yields = 4
  }

  crafting () {
    return [
      null, CopperWire, null,
      CopperWire, Stick, CopperWire,
      null, CopperWire, null
    ]
  }
}
