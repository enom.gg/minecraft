import CopperPlate from './copper-plate'
import SteelSheetmetal from './steel-sheetmetal'
import WaterBucket from './water-bucket'

export default class RadiatorBlock {
  constructor () {
    this.name = 'Radiator Block'
    this.yields = 4
  }

  crafting () {
    return [
      SteelSheetmetal, CopperPlate, SteelSheetmetal,
      CopperPlate, WaterBucket, CopperPlate,
      SteelSheetmetal, CopperPlate, SteelSheetmetal
    ]
  }
}
