<?php

class Item extends Basic {

    public $type = "item";
    public $requires = array();
    public $materials = array();
    public $tree = array();

    public function recipe($class = __CLASS__) {
        $stack = $this->stack;
        $reqs = $this->requires;
        $size = count($reqs);

        for ($i = 0; $i < $size; $i++) {
            $item = $reqs[$i][1];
            $model = $item::model();
            $amount = ceil($reqs[$i][0] / $model->stack);

            if ($model->type == "basic") {
                if (!isset($this->materials[$item]))
                    $this->materials[$item] = 0;
                $this->materials[$item] = + $amount;
            }else if ($model->type == "item") {
                foreach ($model->recipe() as $sItem => $sAmount) {
                    if (!isset($this->materials[$sItem]))
                        $this->materials[$sItem] = 0;
                    $this->materials[$sItem] += $sAmount * (int) ceil($amount / $model->stack);
                }
            }
        }

        return $this->materials;
    }

    public function buildTree() {
        foreach ($this->requires as $req) {
            $amount = $req[0];
            $item = $req[1]::model();

            if ($item->type == "basic") {
                $this->tree[$item->name()] = $amount;
            } else if ($item->type == "item") {
                $this->tree[$item->name()] = array($amount, $item->buildTree());
            }
        }

        return $this->tree;
    }

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Chest extends Item {

    public $require = array(
        array(8, "Wood"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Coal_Dust extends Item {

    public $requires = array(
        array(1, "Coal"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Electronic_Circuit extends Item {

    public $requires = array(
        array(6, "Copper_Cable"),
        array(2, "Redstone"),
        array(1, "Refined_Iron"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Copper_Cable extends Item {

    public $stack = 6;
    public $requires = array(
        array(6, "Rubber"),
        array(3, "Copper"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Furnace extends Item {

    public $requires = array(
        array(8, "Cobblestone"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Machine_Block extends Item {

    public $requires = array(
        array(8, "Refined_Iron"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class RE_Battery extends Item {

    public $requires = array(
        array(4, "Tin"),
        array(2, "Redstone"),
        array(1, "Copper_Cable"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class MGenerator extends Item {

    public $requires = array(
        array(1, "Furnace"),
        array(1, "Machine_Block"),
        array(1, "RE_Battery"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Solar_Panel extends Item {

    public $requires = array(
        array(3, "Coal_Dust"),
        array(3, "Glass"),
        array(2, "Electronic_Circuit"),
        array(1, "MGenerator"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class BatBox extends Item {

    public $requires = array(
        array(5, "Wooden_Planks"),
        array(3, "RE_Battery"),
        array(1, "Copper_Cable"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Wooden_Planks extends Item {

    public $stack = 4;
    public $requires = array(
        array(1, "Wood"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Energy_Condenser extends Item {

    public $requires = array(
        array(4, "Obsidian"),
        array(4, "Diamond"),
        array(1, "Alchemical_Chest"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Alchemical_Chest extends Item {

    public $requires = array(
        array(1, "Covalence_Dust_Green"),
        array(1, "Covalence_Dust_Teal"),
        array(1, "Covalence_Dust_Blue"),
        array(2, "Stone"),
        array(2, "Iron"),
        array(1, "Diamond"),
        array(1, "Chest"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Covalence_Dust_Green extends Item {

    public $stack = 40;
    public $requires = array(
        array(8, "Cobblestone"),
        array(1, "Charcoal"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Covalence_Dust_Teal extends Item {

    public $stack = 40;
    public $requires = array(
        array(1, "Iron"),
        array(1, "Redstone"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Covalence_Dust_Blue extends Item {

    public $stack = 40;
    public $requires = array(
        array(1, "Diamond"),
        array(1, "Coal"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Energy_Collector extends Item {

    public $requires = array(
        array(6, "Glowstone"),
        array(1, "Block_of_Diamond"),
        array(1, "Furnace"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class AntiMatter_Relay extends Item {

    public $requires = array(
        array(7, "Obsidian"),
        array(1, "Block_of_Diamond"),
        array(1, "Glass"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Block_of_Diamond extends Item {

    public $requires = array(
        array(9, "Diamond"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Dark_Matter extends Item {

    public $requires = array(
        array(1, "Philosophers_Stone"),
        array(1, "Block_of_Diamond"),
        array(8, "Aeternalis_Fuel"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Aeternalis_Fuel extends Item {

    public $requires = array(
        #array(1, "Philosophers_Stone"),
        array(4, "Mobius_Fuel"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Mobius_Fuel extends Item {

    public $requires = array(
        #array(1, "Philosophers_Stone"),
        array(4, "Alchemical_Coal"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Alchemical_Coal extends Item {

    public $requires = array(
        #array(1, "Philosophers_Stone"),
        array(4, "Coal"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Philosophers_Stone extends Item {

    public $requires = array(
        array(1, "Diamond"),
        array(4, "Redstone"),
        array(4, "Glowstone"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class High_Voltage_Solar_Array extends Item {

    public $requires = array(
        array(8, "Medium_Voltage_Solar_Array"),
        array(1, "HV_Transformer"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Medium_Voltage_Solar_Array extends Item {

    public $requires = array(
        array(8, "Low_Voltage_Solar_Array"),
        array(1, "MV_Transformer"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Low_Voltage_Solar_Array extends Item {

    public $requires = array(
        array(8, "Solar_Panel"),
        array(1, "LV_Transformer"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class HV_Transformer extends Item {

    public $requires = array(
        array(2, "Ins4x_HV_Cable"),
        array(1, "Electronic_Circuit"),
        array(1, "MV_Transformer"),
        array(1, "Energy_Crystal")
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class MV_Transformer extends Item {

    public $requires = array(
        array(2, "Ins2x_Gold_Cable"),
        array(1, "Machine_Block"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class LV_Transformer extends Item {

    public $requires = array(
        array(4, "Wooden_Planks"),
        array(3, "Copper"),
        array(2, "Copper_Cable"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Ins2x_Gold_Cable extends Item {

    public $requires = array(
        array(2, "Rubber"),
        array(1, "Gold_Cable"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Gold_Cable extends Item {

    public $requires = array(
        array(3, "Gold_Ingot"),
    );
    public $stack = 12;

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Ins4x_HV_Cable extends Item {

    public $requires = array(
        array(3, "Rubber"),
        array(1, "HV_Cable"),
    );

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class HV_Cable extends Item {

    public $requires = array(
        array(3, "Refined_Iron"),
    );
    public $stack = 12;

}

class Energy_Crystal extends Item {
    public $requires = array(
        array(8, "Redstone"),
        array(1, "Diamond"),
    );
    
    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}



?>