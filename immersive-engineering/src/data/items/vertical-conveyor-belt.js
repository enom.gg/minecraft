import ConveyorBelt from './conveyor-belt'
import IronIngot from './iron-ingot'

export default class VerticalConveyorBelt {
  constructor () {
    this.name = 'Vertical Conveyor Belt'
  }

  crafting () {
    return [
      ConveyorBelt, IronIngot, null,
      ConveyorBelt, null, null,
      ConveyorBelt, IronIngot
    ]
  }
}
