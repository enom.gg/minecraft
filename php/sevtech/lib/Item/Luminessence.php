<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;

/**
 *
 */
class Luminessence extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Crafting::AGE_THREE => [
                Crafting::YIELDS => 2,
                Redstone::class => 1,
                Glowstone::class => 1,
                Gunpowder::class => 1,
                StarDust::class => 1,
            ],
        ];
    }
}