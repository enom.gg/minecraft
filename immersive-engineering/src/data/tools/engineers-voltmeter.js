import Compass from '../items/compass'
import CopperIngot from '../items/copper-ingot'
import Stick from '../items/stick'

export default class EngineersVoltmeter {
  constructor () {
    this.name = 'Engineer\'s Voltmeter'
  }

  crafting () {
    return [
      null, null, null,
      null, Compass, null,
      Stick, CopperIngot, Stick
    ]
  }
}
