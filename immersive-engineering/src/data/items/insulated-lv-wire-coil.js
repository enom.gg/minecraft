import LVWireCoil from './lv-wire-coil'
import ToughFabric from './tough-fabric'

export default class InsulatedLVWireCoil {
  constructor () {
    this.name = 'Insulated LV Wire Coil'
    this.yields = 4
  }

  crafting () {
    return [
      ToughFabric, LVWireCoil, ToughFabric,
      LVWireCoil, ToughFabric, LVWireCoil,
      ToughFabric, LVWireCoil, ToughFabric
    ]
  }
}
