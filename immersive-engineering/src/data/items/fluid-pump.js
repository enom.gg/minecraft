import FluidPipe from './fluid-pipe'
import IronMechanicalComponent from './iron-mechanical-component'
import IronPlate from './iron-plate'

export default class FluidPump {
  constructor () {
    this.name = 'Fluid Pump'
  }

  crafting () {
    return [
      null, IronPlate, null,
      IronPlate, IronMechanicalComponent, IronPlate,
      FluidPipe, FluidPipe, FluidPipe
    ]
  }
}
