<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;
use Sevtech\Age;

/**
 *
 */
class BasicCatalyst extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Age\Two::class => [
                Crafting::class => [
                    BasicComponent::class => 4,
                    BlackIronIngot::class => 1,
                ],
            ],
        ];
    }
}