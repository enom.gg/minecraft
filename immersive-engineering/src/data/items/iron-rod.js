import IronIngot from './iron-ingot'

export default class IronRod {
  constructor () {
    this.name = 'Iron Rod'
    this.yields = 4
  }

  crafting () {
    return [
      null, IronIngot,
      null, IronIngot
    ]
  }
}
