<?php

namespace Sevtech;

/**
 *
 */
class Collection
{
    /**
     * Stores our entire collection data.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Adds an item to our collection.
     *
     * @param mixed $item Item to add
     * @param string $key Index key to set
     */
    public function add($item, string $key = null)
    {
        if ($key) {
            $this->data[$key] = $item;
        } else {
            $this->data[] = $item;
        }
    }

    /**
     * Returns all the data collected.
     *
     * @return array
     */
    public function all()
    {
        return $this->data;
    }

    /**
     *
     * @param array $data Data to collect
     * @param \Sevtech\Collection $collection Collection to merge with
     * @return \Sevtech\Collection
     */
    public static function collect(array $data, self $collection = null){
        if ($collection === null) {
            $collection = new self();
        }

        foreach ($data as $value) {
            $collection->add($value);
        }

        return $collection;
    }
}