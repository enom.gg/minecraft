import CircuitBoard from './circuit-board'
import IronIngot from './iron-ingot'
import IronMechanicalComponent from './iron-mechanical-component'
import RedstoneDust from './redstone-dust'
import TreatedWoodPlanks from './treated-wood-planks'

export default class ItemBatcher {
  constructor () {
    this.name = 'Item Batcher'
  }

  crafting () {
    return [
      TreatedWoodPlanks, RedstoneDust, TreatedWoodPlanks,
      IronIngot, IronMechanicalComponent, IronIngot,
      TreatedWoodPlanks, CircuitBoard, TreatedWoodPlanks
    ]
  }
}
