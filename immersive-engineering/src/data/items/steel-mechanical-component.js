import CopperIngot from './copper-ingot'
import SteelPlate from './steel-plate'

export default class SteelMechanicalComponent {
  constructor () {
    this.name = 'Steel Mechanical Component'
  }

  crafting () {
    return [
      SteelPlate, null, SteelPlate,
      null, CopperIngot, null,
      SteelPlate, null, SteelPlate
    ]
  }
}
