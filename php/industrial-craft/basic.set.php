<?php

class Basic {

    private $_data = array();
    public $type = "basic";
    private static $_models = array();
    public $stack = 1;

    public function __get($item) {
        if (isset($this->_data[$item]))
            return $this->_data[$item];
        return null;
    }

    public function __set($item, $value) {
        $this->_data[$item] = $value;
    }

    public static function model($class = __CLASS__) {
        if (!isset(self::$_models[$class]))
            self::$_models[$class] = new $class(null);
        return self::$_models[$class];
    }

    public function name($split = false) {
        if (!$split)
            return get_class($this);
        return trim(implode(" ", explode("_", get_class($this))));
    }

    public function webLink() {
        return "http://wiki.industrial-craft.net/index.php?title=" . $this->name();
    }

}

class Iron extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Glass extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Coal extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Sand extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Rubber extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Copper extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Cobblestone extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Redstone extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Tin extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Wood extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Diamond extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Obsidian extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Stone extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Charcoal extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Glowstone extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Gold_Ingot extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

class Refined_Iron extends Basic {

    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

}

?>