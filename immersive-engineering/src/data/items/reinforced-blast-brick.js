import BlastBrick from './blast-brick'
import SteelPlate from './steel-plate'

export default class ReinforcedBlastBrick {
  constructor () {
    this.name = 'Reinforced Blast Brick'
  }

  crafting () {
    return [
      BlastBrick, SteelPlate
    ]
  }
}
