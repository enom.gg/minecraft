import CopperIngot from './copper-ingot'
import IronIngot from './iron-ingot'
import LeadIngot from './lead-ingot'
import RedstoneDust from './redstone-dust'
import TreatedWoodPlanks from './treated-wood-planks'

export default class LVCapacitor {
  constructor () {
    this.name = 'LV Capacitor'
  }

  crafting () {
    return [
      IronIngot, IronIngot, IronIngot,
      CopperIngot, LeadIngot, CopperIngot,
      TreatedWoodPlanks, RedstoneDust, TreatedWoodPlanks
    ]
  }
}
