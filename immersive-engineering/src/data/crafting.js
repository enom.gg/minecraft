import { createComputed, createContext, createState, untrack, useContext } from 'solid-js'

import * as items from './items/*.js'
import * as multi from './multiblock/*.js'
import * as tools from './tools/*.js'
import { use as router } from '--/router'

const CraftingContext = createContext()
const make = (files) => Object.values(files.default)
  .map(({ default: Construstable }) => new Construstable())
  .sort((a, b) => a.name > b.name)
const named = (name) => (item) => item.name === name
const newed = (constructor) => (item) => item.constructor === constructor

export function CraftingProvider (props) {
  const { change, parse } = router()
  const [getState, setState] = createState({ crafting: parse(), materials: [] })
  const state = {
    items: make(items),
    multi: make(multi),
    state: getState,
    tools: make(tools)
  }
  const store = [state.items, state.multi, state.tools].flat()
  const commit = (name, quantity, force = false) => {
    if (typeof quantity === 'string') quantity = parseInt(quantity, 10)
    if (Number.isNaN(quantity)) quantity = 0

    const index = getState.crafting.findIndex(named(name))

    if (index === -1) {
      if (quantity) setState('crafting', (list) => [...list, { name, quantity }])
    } else {
      setState('crafting', index, 'quantity', (value) => force ? quantity : value + quantity)

      if (!getState.crafting[index].quantity) {
        setState('crafting', (list) => [...list.slice(0, index), ...list.slice(index + 1)])
      }
    }

    change(getState.crafting)
  }
  const actions = {
    add: (name) => commit(name, 1),
    materials: (item, quantity = 1) => reduce(item, quantity, store),
    named: (name) => store.find(named(name)),
    newed: (constructor) => store.find(newed(constructor)),
    remove: (name) => commit(name, -1),
    update: (name, quantity) => commit(name, quantity, true)
  }
  const reduce = (material) => {
    const { crafting, tool, yields } = actions.named(material.name)

    if (crafting && !tool) {
      crafting().reduce((materials, slot) => {
        if (!slot) return materials

        const { name, tool } = actions.newed(slot)
        const existing = materials.find(named(name))

        if (existing) {
          if (!tool) existing.quantity++
        } else {
          materials = [...materials, { name, quantity: 1 }]
        }

        return materials
      }, []).forEach((reduced) => {
        const item = actions.named(reduced.name)
        const multiplier = Math.ceil(material.quantity / (yields || 1))

        reduce({ name: reduced.name, quantity: item.tool ? 1 : reduced.quantity * multiplier })
      })
    } else {
      const index = untrack(() => getState.materials.findIndex(named(material.name)))

      if (index === -1) {
        setState('materials', (materials) => [...materials, material])
      } else {
        if (!tool) setState('materials', index, 'quantity', (v) => v + material.quantity)
      }
    }
  }

  createComputed(() => {
    setState('materials', [])

    getState.crafting.forEach(reduce)
  })

  return (
    <CraftingContext.Provider value={[state, actions]}>
      {props.children}
    </CraftingContext.Provider>
  )
}

export function use () {
  return useContext(CraftingContext)
}
