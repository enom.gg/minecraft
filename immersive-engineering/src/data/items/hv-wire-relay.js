import AluminiumIngot from './aluminium-ingot'
import InsulatingGlass from './insulating-glass'

export default class HVWireRelay {
  constructor () {
    this.name = 'HV Wire Relay'
    this.yields = 8
  }

  crafting () {
    return [
      null, AluminiumIngot, null,
      InsulatingGlass, AluminiumIngot, InsulatingGlass,
      InsulatingGlass, AluminiumIngot, InsulatingGlass
    ]
  }
}
