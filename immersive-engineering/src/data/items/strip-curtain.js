import IronRod from './iron-rod'
import ToughFabric from './tough-fabric'

export default class StripCurtain {
  constructor () {
    this.name = 'Strip Curtain'
    this.yields = 3
  }

  crafting () {
    return [
      IronRod, IronRod, IronRod,
      ToughFabric, ToughFabric, ToughFabric,
      ToughFabric, ToughFabric, ToughFabric
    ]
  }
}
