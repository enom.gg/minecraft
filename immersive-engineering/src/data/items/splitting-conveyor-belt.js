import ConveyorBelt from './conveyor-belt'
import IronIngot from './iron-ingot'

export default class SplittingConveyorBelt {
  constructor () {
    this.name = 'Splitting Conveyor Belt'
    this.yields = 3
  }

  crafting () {
    return [
      null, null, null,
      ConveyorBelt, IronIngot, ConveyorBelt,
      null, ConveyorBelt, null
    ]
  }
}
