<?php

namespace Sevtech\Age;

/**
 *
 */
class Age
{

    /**
     * Print friendly age name.
     *
     * @var string
     */
    protected $name = 'Zero';

    /**
     * Returns an array index friendly age name.
     *
     * @return string
     */
    public function age()
    {
        return get_class($this);
    }

    /**
     * Returns a print friendly age name.
     *
     * @return string
     */
    public function name()
    {
        return 'Age ' . $this->name;
    }

}