<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class GoldPlate extends Item implements Process\Process
{
    ///
    public function process()
    {
        return [
            Process\Casting::PLATE => GoldIngot::class,
            Process\MetalPress::PLATE => GoldIngot::class,
            Process\StoneAnvil::class => GoldIngot::class,
        ];
    }
}