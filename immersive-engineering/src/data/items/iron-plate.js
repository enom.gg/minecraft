import EngineersHammer from '../tools/engineers-hammer'
import IronIngot from './iron-ingot'

export default class IronPlate {
  constructor () {
    this.name = 'Iron Plate'
  }

  crafting () {
    return [
      IronIngot, EngineersHammer
    ]
  }
}
