import TreatedWoodPlanks from './treated-wood-planks'

export default class TreatedStick {
  constructor () {
    this.name = 'Treated Stick'
    this.yields = 4
  }

  crafting () {
    return [
      null, TreatedWoodPlanks,
      null, TreatedWoodPlanks
    ]
  }
}
