import SteelIngot from './steel-ingot'
import SteelRod from './steel-rod'

export default class SteelScaffolding {
  constructor () {
    this.name = 'Steel Scaffolding'
    this.yields = 6
  }

  crafting () {
    return [
      SteelIngot, SteelIngot, SteelIngot,
      null, SteelRod, null,
      SteelRod, null, SteelRod
    ]
  }
}
