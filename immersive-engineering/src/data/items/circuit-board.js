import CopperPlate from './copper-plate'
import InsulatedGlass from './insulating-glass'
import VacuumTube from './vacuum-tube'

export default class CircuitBoard {
  constructor () {
    this.name = 'Circuit Board'
  }

  crafting () {
    return [
      InsulatedGlass, CopperPlate,
      VacuumTube, null
    ]
  }
}
