import ConveyorBelt from './conveyor-belt'
import IronMechanicalComponent from './iron-mechanical-component'
import StripCurtain from './strip-curtain'
import TreatedWoodPlanks from './treated-wood-planks'

export default class ExtractingConveyorBelt {
  constructor () {
    this.name = 'Extracting Conveyor Belt'
  }

  crafting () {
    return [
      TreatedWoodPlanks, StripCurtain,
      IronMechanicalComponent, ConveyorBelt
    ]
  }
}
