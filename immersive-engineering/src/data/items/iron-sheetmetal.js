import IronPlate from './iron-plate'

export default class IronSheetmetal {
  constructor () {
    this.name = 'Iron Sheetmetal'
    this.yields = 4
  }

  crafting () {
    return [
      null, IronPlate, null,
      IronPlate, null, IronPlate,
      null, IronPlate, null
    ]
  }
}
