import { wants } from '--/utils'
import EngineersHammer from '../tools/engineers-hammer'
import FluidPipe from '../items/fluid-pipe'
import HeavyEngineeringBlock from '../items/heavy-engineering-block'
import IronSheetmetal from '../items/iron-sheetmetal'
import LightEngineeringBlock from '../items/light-engineering-block'
import RedstoneEngineeringBlock from '../items/redstone-engineering-block'
import SteelScaffolding from '../items/steel-scaffolding'

export default class Refinery {
  constructor () {
    this.multi = true
    this.name = 'Refinery'
  }

  crafting () {
    return [
      EngineersHammer,
      wants(8, SteelScaffolding),
      wants(2, LightEngineeringBlock),
      wants(2, HeavyEngineeringBlock),
      wants(16, IronSheetmetal),
      wants(1, RedstoneEngineeringBlock),
      wants(5, FluidPipe)
    ].flat()
  }
}
