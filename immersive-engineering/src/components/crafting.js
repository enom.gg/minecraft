import { For } from 'solid-js/dom'

import { use as data } from '--/data/crafting'

export default function Crafting (props) {
  const [, { named, newed }] = data()
  const item = named(props.name)
  const crafting = item.crafting()
  const shapeless = crafting.length <= 4
  const shaped = crafting.length > 4 && crafting.length <= 9
  const place = (name) => name.split(' ').reduce((str, word) => str + word[0], '')

  return (shaped || shapeless) && (
    <div
      class='grid content-center text-center'
      classList={{
        'grid-cols-3 grid-rows-3': shaped,
        'grid-cols-2 grid-rows-2': shapeless
      }}
    >
      <For each={crafting}>
        {(material, i) => (
          <div
            classList={{
              'border-t': (shaped && i() >= 3) || (shapeless && i() >= 2),
              'border-l': (shaped && i() % 3 > 0) || (shapeless && i() % 2 > 0)
            }}
          > {material && place(newed(material).name)}
          </div>
        )}
      </For>
    </div>
  )
}
