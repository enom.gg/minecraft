<?php

namespace Sevtech;

use Sevtech\Item\Item;

/**
 *
 */

class Sevtech
{

    /**
     * Returns the class name without it's namespace and/or extension.
     *
     * @param string $class Class name to base
     * @return string
     */
    public static function basename(string $class){
        if (($last = strrpos($class, '\\')) !== false) {
            return substr($class,  $last + 1);
        } else {
            return $class;
        }
    }

    /**
     * Returns the class name with spaces between words.
     *
     * @param string $class Class name to split
     * @return string
     */
    public static function splitname(string $class){
        return trim(preg_replace('/([A-Z])/', ' $1', $class));
    }

    /**
     * Returns the current $_GET[item] class.
     *
     * @return \Sevtech\Item\Item|null
     * @throws \ReflectionException
     */
    public static function item(){
        // Don't load empty GET request
        if (!($base = $_GET['item'] ?? false)) {
            return null;
        }

        // Used to find the fully qualified class name
        $ref = new \ReflectionClass(Item::class);
        $class = $ref->getNamespaceName() . '\\' . $base;

        // Only instantiate valid classes
        return class_exists($class) ? new $class() : null;
    }

    /**
     * Returns the list of items available.
     *
     * @param bool $sort Sort items alphebetically
     * @return array
     * @throws \ReflectionException
     */
    public static function items(bool $sort = true){
        // Used to find children in the same folder
        $ref = new \ReflectionClass(Item::class);
        $ns = $ref->getNamespaceName();
        $items = [];
        $ignore = ['.', '..', basename($ref->getFileName())];

        if ($handle = opendir(dirname($ref->getFileName()))) {
            while (false !== ($name = readdir($handle))) {
                if (!in_array($name, $ignore)) {
                    $class = $ns . '\\' . basename($name, '.php');
                    $items[] = new $class();
                }
            }

            closedir($handle);
        }

        if ($sort) {
            usort($items, static function (Item $a, Item $b) {
                return $a->name() <=> $b->name();
            });
        }

        return $items;
    }
}