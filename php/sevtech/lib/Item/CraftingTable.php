<?php

namespace Sevtech\Item;

use Sevtech\Process\Crafting;

/**
 *
 */
class CraftingTable extends Item implements Crafting
{
    ///
    public function process()
    {
        return [
            Crafting::AGE_ONE => [
                WoodenPlanks::class => 4,
            ],
        ];
    }
}