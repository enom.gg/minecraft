<?php

namespace Sevtech\Item;

use Sevtech\Process;

/**
 *
 */
class IronPlate extends Item implements Process\Process
{
    ///
    public function process()
    {
        return [
            Process\Casting::PLATE => IronIngot::class,
            Process\MetalPress::PLATE => IronIngot::class,
            Process\StoneAnvil::class => IronIngot::class,
        ];
    }
}