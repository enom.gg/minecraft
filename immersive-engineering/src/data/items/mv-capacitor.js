import BlockOfRedstone from './block-of-redstone'
import ElectrumIngot from './electrum-ingot'
import IronIngot from './iron-ingot'
import LeadIngot from './lead-ingot'
import TreatedWoodPlanks from './treated-wood-planks'

export default class MVCapacitor {
  constructor () {
    this.name = 'MV Capacitor'
  }

  crafting () {
    return [
      IronIngot, IronIngot, IronIngot,
      ElectrumIngot, LeadIngot, ElectrumIngot,
      TreatedWoodPlanks, BlockOfRedstone, TreatedWoodPlanks
    ]
  }
}
