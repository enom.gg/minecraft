import ConveyorBelt from './conveyor-belt'
import IronIngot from './iron-ingot'
import IronMechanicalComponent from './iron-mechanical-component'
import RedstoneDust from './redstone-dust'
import TreatedWoodPlanks from './treated-wood-planks'

export default class ItemRouter {
  constructor () {
    this.name = 'Item Router'
  }

  crafting () {
    return [
      TreatedWoodPlanks, RedstoneDust, TreatedWoodPlanks,
      IronIngot, IronMechanicalComponent, IronIngot,
      TreatedWoodPlanks, ConveyorBelt, TreatedWoodPlanks
    ]
  }
}
