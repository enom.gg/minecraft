import Brick from './brick'
import ClayBall from './clay-ball'
import Sandstone from './sandstone'

export default class CokeBrick {
  constructor () {
    this.name = 'Coke Brick'
    this.yields = 3
  }

  crafting () {
    return [
      ClayBall, Brick, ClayBall,
      Brick, Sandstone, Brick,
      ClayBall, Brick, ClayBall
    ]
  }
}
