import ElectrumCoilBlock from './electrum-coil-block'
import IronIngot from './iron-ingot'
import LVWireConnector from './lv-wire-connector'
import MVWireConnector from './mv-wire-connector'

export default class Transformer {
  constructor () {
    this.name = 'Transformer'
  }

  crafting () {
    return [
      LVWireConnector, null, MVWireConnector,
      IronIngot, ElectrumCoilBlock, IronIngot,
      IronIngot, IronIngot, IronIngot
    ]
  }
}
