import Cobblestone from './cobblestone'
import IronIngot from './iron-ingot'
import OakPlanks from './oak-planks'
import RedstoneDust from './redstone-dust'

export default class Piston {
  constructor () {
    this.name = 'Piston'
  }

  crafting () {
    return [
      OakPlanks, OakPlanks, OakPlanks,
      Cobblestone, IronIngot, Cobblestone,
      Cobblestone, RedstoneDust, Cobblestone
    ]
  }
}
