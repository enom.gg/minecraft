import FluidPipe from './fluid-pipe'
import IronIngot from './iron-ingot'
import IronMechanicalComponent from './iron-mechanical-component'
import RedstoneDust from './redstone-dust'
import TreatedWoodPlanks from './treated-wood-planks'

export default class FluidRouter {
  constructor () {
    this.name = 'Fluid Router'
  }

  crafting () {
    return [
      TreatedWoodPlanks, RedstoneDust, TreatedWoodPlanks,
      IronIngot, IronMechanicalComponent, IronIngot,
      TreatedWoodPlanks, FluidPipe, TreatedWoodPlanks
    ]
  }
}
