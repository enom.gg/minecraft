import RedstoneDust from './redstone-dust'

export default class BlockOfRedstone {
  constructor () {
    this.name = 'Block of Redstone'
  }

  crafting () {
    return [
      RedstoneDust, RedstoneDust, RedstoneDust,
      RedstoneDust, RedstoneDust, RedstoneDust,
      RedstoneDust, RedstoneDust, RedstoneDust
    ]
  }
}
