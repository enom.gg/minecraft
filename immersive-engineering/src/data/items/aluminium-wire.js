import AluminiumPlate from './aluminium-plate'
import EngineersWireCutters from '../tools/engineers-wire-cutters'

export default class AluminiumWire {
  constructor () {
    this.name = 'Aluminium Wire'
  }

  crafting () {
    return [
      AluminiumPlate, EngineersWireCutters
    ]
  }
}
