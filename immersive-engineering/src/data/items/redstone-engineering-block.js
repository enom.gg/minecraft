import CopperIngot from './copper-ingot'
import IronSheetmetal from './iron-sheetmetal'
import RedstoneDust from './redstone-dust'

export default class RedstoneEngineeringBlock {
  constructor () {
    this.name = 'Redstone Engineering Block'
    this.yields = 4
  }

  crafting () {
    return [
      IronSheetmetal, RedstoneDust, IronSheetmetal,
      RedstoneDust, CopperIngot, RedstoneDust,
      IronSheetmetal, RedstoneDust, IronSheetmetal
    ]
  }
}
