<?php

namespace Sevtech\Age;

/**
 *
 */
class Two extends Age
{

    /**
     * Print friendly age name.
     *
     * @var string
     */
    protected $name = 'Two';

}