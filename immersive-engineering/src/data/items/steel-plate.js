import EngineersHammer from '../tools/engineers-hammer'
import SteelIngot from './steel-ingot'

export default class SteelPlate {
  constructor () {
    this.name = 'Steel Plate'
  }

  crafting () {
    return [
      SteelIngot, EngineersHammer
    ]
  }
}
