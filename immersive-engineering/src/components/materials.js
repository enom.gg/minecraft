import { For } from 'solid-js/dom'

import { use as data } from '--/data/crafting'

export default function Materials () {
  const [{ state }, { named }] = data()
  const materials = () => state.materials
    .filter((material) => !named(material.name).tool)
    .sort((a, b) => a.name > b.name)

  return (
    <>
      <h3 class='py-2 text-xl font-bold text-gray-600'>Materials</h3>

      <div class='grid grid-cols-12 gap-1'>
        <For
          each={materials()}
          fallback={(
            <div class='col-start-2 col-span-11 text-gray-400'>Nothing selected</div>
          )}
        >
          {(item) => (
            <>
              <div class='col-span-3 text-center pr-2'>
                {item.quantity}
              </div>

              <div class='col-span-9'>
                {item.name}
              </div>
            </>
          )}
        </For>
      </div>
    </>
  )
}
